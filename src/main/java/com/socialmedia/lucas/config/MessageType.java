package com.socialmedia.lucas.config;

public enum MessageType {
    Success,
    Error
}
