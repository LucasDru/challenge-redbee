package com.socialmedia.lucas.config;

import java.util.ArrayList;
import java.util.List;

public class Response {

    public List<Message> messages;

    public Response(){
        this.messages = new ArrayList<>();
    }

    public void AddError(String msg){
        this.messages.add(new Message(msg, MessageType.Error));
    }

    public void AddSucces(String msg){
        this.messages.add(new Message(msg, MessageType.Success));
    }

    public boolean HasErrors(){
        return this.messages.stream().anyMatch(x -> x.type == MessageType.Error);
    }
}
