package com.socialmedia.lucas.models;

public class TweetModel {
    public String text;
    public String user;

    public TweetModel(String text, String user) {
        this.text = text;
        this.user = user;
    }
}
