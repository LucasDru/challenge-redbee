package com.socialmedia.lucas.models;

import java.util.List;

public class BoardModel {

    public String boardId;
    public String title;
    public List<TweetModel> tweets;
    public String[] peoples;
    public String[] interests;
}
