package com.socialmedia.lucas.servicies;

import com.socialmedia.lucas.entities.Board;
import com.socialmedia.lucas.entities.Tweet;
import com.socialmedia.lucas.entities.User;
import com.socialmedia.lucas.models.BoardModel;
import com.socialmedia.lucas.models.TweetModel;
import com.socialmedia.lucas.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class BoardService {

    @Autowired
    private UserRepository userRepository;

    public List<BoardModel> GetByUserUsername(String username) {
        User user = userRepository.findByUsername(username);

        HashMap<String, Board> boards = user.getBoards();

        List<BoardModel> boardListModel = new ArrayList<>();

        boards.forEach((key, board) -> {
            BoardModel boardModel = new BoardModel();
            boardModel.boardId = key;
            boardModel.title = board.getTitle();
            boardModel.interests = board.getInterests().split(";");
            boardModel.peoples = board.getPeoples().split(";");

            boardModel.tweets = new ArrayList<>();

            board.getTweets().forEach(tweet -> {
                if(!tweet.isSeen()){
                    boardModel.tweets.add(new TweetModel(tweet.getText(), tweet.getUser()));
                }

                tweet.setSeen(true);
            });

            boardListModel.add(boardModel);
        });

        this.userRepository.save(user);

        return boardListModel;
    }
}
