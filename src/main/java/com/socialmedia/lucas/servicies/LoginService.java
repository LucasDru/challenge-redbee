package com.socialmedia.lucas.servicies;

import com.socialmedia.lucas.config.Response;
import com.socialmedia.lucas.models.LoginModel;
import com.socialmedia.lucas.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginService {

    @Autowired
    private UserRepository userRepository;

    public Response Login(LoginModel model){
        Response response = new Response();

        if(userRepository.existsByUsernameAndPassword(model.username, model.password)){
            response.AddSucces("Login Correcto");
        }
        else{
            response.AddError("Usuario o Contraseña no incorecta");
        }

        return response;
    }
}
