package com.socialmedia.lucas.servicies;

import com.socialmedia.lucas.entities.Board;
import com.socialmedia.lucas.entities.Tweet;
import com.socialmedia.lucas.entities.User;
import com.socialmedia.lucas.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.social.twitter.api.SearchResults;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

@Component
public class TwitterScheduledService {

    @Autowired
    UserRepository userRepository;

    private final TwitterTemplate twitterTemplate;

    public TwitterScheduledService(TwitterTemplate twitterTemplate) {
        this.twitterTemplate=twitterTemplate;
    }

    private int tweetQuantity = 2;

    @Scheduled(fixedRate = 300000)
    public void UpdateTweetsOnBoards(){
       List<User> users = userRepository.findAll();

        for (User user: users) {

            HashMap<String, Board> boards = user.getBoards();

            boards.forEach((key, board) -> {

                this.UpdateTweets(board.getInterests(), board);
                this.UpdateTweets(board.getPeoples(), board);
            });

            this.userRepository.save(user);
        }
    }

    private void UpdateTweets(String query, Board board){

        if(!query.isEmpty()){
            query = query.replace(";", " OR ");
            List<org.springframework.social.twitter.api.Tweet> tweets = twitterTemplate.searchOperations().search(query, tweetQuantity).getTweets();

            for (org.springframework.social.twitter.api.Tweet tweet: tweets) {
                Tweet entity = new Tweet(tweet.getText(), tweet.getFromUser());

                board.AddTweet(entity);
            }
        }
    }
}
