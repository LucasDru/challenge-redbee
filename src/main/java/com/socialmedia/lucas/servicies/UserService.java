package com.socialmedia.lucas.servicies;

import com.socialmedia.lucas.config.Response;
import com.socialmedia.lucas.entities.Board;
import com.socialmedia.lucas.entities.User;
import com.socialmedia.lucas.models.BoardAddModel;
import com.socialmedia.lucas.models.BoardUpdateModel;
import com.socialmedia.lucas.models.UserAddModel;
import com.socialmedia.lucas.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public Response Add(UserAddModel model){
        Response response = new Response();

        if(userRepository.existsByUsername(model.username)){
            response.AddError("Nombre de usuario ya existe");
            return response;
        }

        User user = new User();

        user.setUsername(model.username);
        user.setPassword(model.password);

        try {
            userRepository.insert(user);
            response.AddSucces("Usuario creado correctamente");
        }
        catch (Exception e){
            response.AddError("Ocurrio un error al crear el usuario");
        }

        return response;
    }

    public Response AddBoard(BoardAddModel model){
        Response response = new Response();
        User user = userRepository.findByUsername(model.username);

        if(user == null){
            response.AddError("Usuario no encontrado");
            return response;
        }

        Board board = new Board(model.title, model.peoples, model.interests);
        user.AddBoard(board);

        try {
            userRepository.save(user);
            response.AddSucces("Board creado correctamente");
        }
        catch (Exception e){
            response.AddError("Ocurrio un error al crear el usuario");
        }

        return response;
    }

    public Response UpdateBoard(BoardUpdateModel model) {
        Response response = new Response();
        User user = userRepository.findByUsername(model.username);

        if(user == null){
            response.AddError("Usuario no encontrado");
            return response;
        }

        HashMap<String, Board> boards = user.getBoards();

        Board board = boards.get(model.boardId);
        board.setTitle(model.title);
        board.setPeoples(model.peoples);
        board.setInterests(model.interests);

        try {
            userRepository.save(user);
            response.AddSucces("Board actualizado correctamente");
        }
        catch (Exception e){
            response.AddError("Ocurrio un error al crear el usuario");
        }

        return response;
    }
}
