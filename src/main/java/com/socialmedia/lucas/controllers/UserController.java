package com.socialmedia.lucas.controllers;

import com.socialmedia.lucas.config.Response;
import com.socialmedia.lucas.models.UserAddModel;
import com.socialmedia.lucas.servicies.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/users")
@CrossOrigin(origins = "*")
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Response> Post(@RequestBody UserAddModel model){
        Response response = userService.Add(model);

        if(response.HasErrors()) return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
