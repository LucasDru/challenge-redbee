package com.socialmedia.lucas.controllers;

import com.socialmedia.lucas.config.Response;
import com.socialmedia.lucas.models.BoardAddModel;
import com.socialmedia.lucas.models.BoardModel;
import com.socialmedia.lucas.models.BoardUpdateModel;
import com.socialmedia.lucas.servicies.BoardService;
import com.socialmedia.lucas.servicies.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/boards")
@CrossOrigin(origins = "*")
public class BoardController {

    @Autowired
    UserService userService;

    @Autowired
    BoardService boardService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Response> Post(@RequestBody BoardAddModel model){
        Response response = userService.AddBoard(model);

        if(response.HasErrors()) return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<Response> Put(@RequestBody BoardUpdateModel model){
        Response response = userService.UpdateBoard(model);

        if(response.HasErrors()) return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/{username}/tweets", method = RequestMethod.GET)
    public ResponseEntity<List<BoardModel>> GetByUserId(@PathVariable("username") String username){
        List<BoardModel> list = boardService.GetByUserUsername(username);

        return new ResponseEntity<>(list, HttpStatus.OK);
    }
}
