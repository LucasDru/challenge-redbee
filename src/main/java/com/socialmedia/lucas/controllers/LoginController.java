package com.socialmedia.lucas.controllers;

import com.socialmedia.lucas.config.Response;
import com.socialmedia.lucas.models.LoginModel;
import com.socialmedia.lucas.servicies.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/login")
@CrossOrigin(origins = "*")
public class LoginController {

    @Autowired
    LoginService loginService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Response> Post(@RequestBody LoginModel model){
        Response response = loginService.Login(model);

        if(response.HasErrors()) return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
