package com.socialmedia.lucas.repositories;

import com.socialmedia.lucas.entities.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, String> {
    boolean existsByUsernameAndPassword(String username, String password);
    boolean existsByUsername(String username);
    User findByUsername(String username);
}

