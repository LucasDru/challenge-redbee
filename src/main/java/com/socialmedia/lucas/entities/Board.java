package com.socialmedia.lucas.entities;

import java.util.ArrayList;
import java.util.List;

public class Board {

    String title;

    String peoples;

    String interests;

    List<Tweet> tweets;

    public Board(String title, String peoples, String interests) {
        this.title = title;
        this.peoples = peoples;
        this.interests = interests;
        this.tweets = new ArrayList<>();
    }

    public Board(){
        this.tweets = new ArrayList<>();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPeoples() {
        return peoples;
    }

    public List<Tweet> getTweets() {
        return tweets;
    }

    public void setPeoples(String peoples) {
        this.peoples = peoples;
    }

    public String getInterests() {
        return interests;
    }

    public void setInterests(String interests) {
        this.interests = interests;
    }

    public void AddTweet(Tweet tweet){
        this.tweets.add(tweet);
    }
}
