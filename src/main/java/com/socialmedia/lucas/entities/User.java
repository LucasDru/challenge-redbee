package com.socialmedia.lucas.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;

@Document(collection = "users")
public class User {

    @Id
    String id;

    String username;

    String password;

    HashMap<String, Board> boards;

    public User() {
        this.boards = new HashMap<>();
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
        this.boards = new HashMap<>();
    }

    public void setUsername(String username) { this.username = username; }

    public void setPassword(String password) { this.password = password; }

    public HashMap<String, Board> getBoards(){
        return this.boards;
    }

    public void AddBoard(Board board){
        this.boards.put(java.util.UUID.randomUUID().toString(), board);
    }
}
