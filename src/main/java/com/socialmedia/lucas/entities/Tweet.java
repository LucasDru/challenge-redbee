package com.socialmedia.lucas.entities;

public class Tweet {

    private String text;

    private String user;

    private boolean seen;

    public Tweet(){
    }

    public Tweet(String text, String user) {
        this.text = text;
        this.user = user;
        this.seen = false;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public boolean isSeen() {
        return seen;
    }

    public String getText() {
        return text;
    }

    public String getUser() {
        return user;
    }
}
